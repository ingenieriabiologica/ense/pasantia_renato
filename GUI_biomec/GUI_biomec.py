# -*- coding: utf-8 -*-
import sys
from PyQt5 import uic, QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
import seaborn as sns
import biomec
import pandas as pd
import numpy as np
import scipy as sc

class main_GUI(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("./GUI_biomec.ui", self)
        self.tabWidget.setTabEnabled(1, False)
        self.tabWidget.setTabEnabled(2, False)
        self.tabWidget.setTabEnabled(3, False)
        self.button_energ.setEnabled(False)
        self.button_win.setEnabled(False)
        self.button_stiffness.setEnabled(False)
        self.guardar_wint.setEnabled(False)
        self.guardar_energ.setEnabled(False)
        self.guardar_rig.setEnabled(False)
        self.button_select_files.clicked.connect(self.fn_select_files)
        self.button_win.clicked.connect(self.fn_win)
        self.button_energ.clicked.connect(self.fn_energ)
        self.button_stiffness.clicked.connect(self.fn_stiffness)
        self.guardar_wint.clicked.connect(self.save_win)
        self.guardar_energ.clicked.connect(self.save_energ)
        self.guardar_rig.clicked.connect(self.save_rig)
        
        self.c_energ = -1
        self.c_win = -1
        self.c_rig = -1
        
        self.pbar_win.setValue(0)
        self.pbar_energ.setValue(0)
        self.pbar_stiffness.setValue(0)
        self.label_files.setText("Ningún archivo seleccionado")
        
        self.show()
    
    def fn_select_files(self):
        self.tabWidget.setTabEnabled(1, False)
        self.tabWidget.setTabEnabled(2, False)
        self.tabWidget.setTabEnabled(3, False)
        self.pbar_win.setValue(0)
        self.pbar_energ.setValue(0)
        self.pbar_stiffness.setValue(0)
        options = QtWidgets.QFileDialog.Options()
        filepath = QtWidgets.QFileDialog.getOpenFileNames(self, "Seleccione un archivo", "","Archivos NPY (*.npy);; All files (*.)", options=options)
        if not filepath[0]:
            return
        self.label_files.setText(str(len(filepath[0])) + " archivos seleccionados")
        self.files = filepath[0]
        self.button_energ.setEnabled(True)
        self.button_win.setEnabled(True)
        self.button_stiffness.setEnabled(True)
    
    def saveFileDialog(self):
        options = QtWidgets.QFileDialog.Options()
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","NPY (*.npy);;CSV (*.csv);;Pickle ()", options=options)
        return fileName
    
    def save_win(self):
        fileName = self.saveFileDialog()
        if fileName:
            if fileName[-3:]=='npy':
                np.save(fileName, self.win)
            elif fileName[-3:]=='csv':
                self.win.to_csv(fileName)
            else:
                self.win.to_pickle(fileName[:-4])
        else:
            return
    def save_energ(self):
        fileName = self.saveFileDialog()
        if fileName:
            if fileName[-3:]=='npy':
                np.save(fileName, self.energ)
            elif fileName[-3:]=='csv':
                self.energ.to_csv(fileName)
            else:
                self.energ.to_pickle(fileName[:-4])
        else:
            return
        
    def save_rig(self):
        fileName = self.saveFileDialog()
        if fileName:
            if fileName[-3:]=='npy':
                np.save(fileName, self.rig)
            elif fileName[-3:]=='csv':
                self.rig.to_csv(fileName)
            elif fileName[-3:] =='mat':
                df = {name: col.values for name, col in self.rig.items()}
                sc.io.savemat(fileName, df)
            else:
                self.rig.to_pickle(fileName[:-4])
        else:
            return
        
    def fn_win(self):
        self.act_win.clicked.connect(self.actualizar_win)
        self.pbar_win.setValue(0)
        listdf = []
        Wintbyframe = {} 
        N = len(self.files)
        for i in range(N):
            WINT, byframe, info = biomec.wint(self.files[i])
            Wintbyframe.update({info.file_name.values[0]:byframe})
            listdf.append(WINT)
            progress = int((i+1)/N*100)
            self.pbar_win.setValue(progress)
            
        Wint = pd.concat(listdf,ignore_index=True)
        self.tabWidget.setTabEnabled(1, True)
        self.win = Wint
        
        self.win_eje_x_1.addItems(Wint.columns)
        self.win_eje_y_1.addItems(Wint.columns)
        self.win_agrupacion_1.addItems(Wint.columns)
        self.win_eje_x_2.addItems(Wint.columns)
        self.win_eje_y_2.addItems(Wint.columns)
        self.win_agrupacion_2.addItems(Wint.columns)
        self.win_eje_x_3.addItems(Wint.columns)
        self.win_eje_y_3.addItems(Wint.columns)
        self.win_agrupacion_3.addItems(Wint.columns)
        
        self.CB_win_1.setChecked(True)
        self.CB_win_2.setChecked(False)
        self.CB_win_3.setChecked(False)
        
        index_1 = self.win_eje_x_1.findText('vel')
        index_2 = self.win_eje_y_1.findText('Wint')
        index_3 = self.win_agrupacion_1.findText('gait')
        
        self.win_eje_x_1.setCurrentIndex(index_1)
        self.win_eje_x_2.setCurrentIndex(index_1)
        self.win_eje_x_3.setCurrentIndex(index_1)
        self.win_eje_y_1.setCurrentIndex(index_2)
        self.win_agrupacion_1.setCurrentIndex(index_3)
        
        if self.c_win==0:
            self.fig_win.itemAt(0).widget().deleteLater()
        else:    
            self.c_win = 0
        
        self.graficar(ejes = [['vel', 'Wint', 'gait']], df = Wint, layout = self.fig_win)        
        self.guardar_wint.setEnabled(True)
        
    def fn_energ(self):
        self.act_energ.clicked.connect(self.actualizar_energ)
        self.pbar_energ.setValue(0)
        N = len(self.files)
        r = []
        for i in range(N): 
            energ_param = biomec.energetic(self.files[i])
            r.append(energ_param)
            progress = int((i+1)/N*100)
            self.pbar_energ.setValue(progress)

        alldata = pd.concat(r, ignore_index = True)
        self.energ = alldata
        
        self.energ_eje_x_1.addItems(alldata.columns)
        self.energ_eje_y_1.addItems(alldata.columns)
        self.energ_agrupacion_1.addItems(alldata.columns)
        self.energ_eje_x_2.addItems(alldata.columns)
        self.energ_eje_y_2.addItems(alldata.columns)
        self.energ_agrupacion_2.addItems(alldata.columns)
        self.energ_eje_x_3.addItems(alldata.columns)
        self.energ_eje_y_3.addItems(alldata.columns)
        self.energ_agrupacion_3.addItems(alldata.columns)
        
        self.tabWidget.setTabEnabled(2, True)
        
        self.CB_energ_1.setChecked(True)
        self.CB_energ_2.setChecked(False)
        self.CB_energ_3.setChecked(False)
        index_1 = self.energ_eje_x_1.findText('vel')
        index_2 = self.energ_eje_y_1.findText('recovery')
        index_3 = self.energ_agrupacion_1.findText('gait')
        
        self.energ_eje_x_1.setCurrentIndex(index_1)
        self.energ_eje_x_2.setCurrentIndex(index_1)
        self.energ_eje_x_3.setCurrentIndex(index_1)
        self.energ_eje_y_1.setCurrentIndex(index_2)
        self.energ_agrupacion_1.setCurrentIndex(index_3)
        
        if self.c_energ==0:
            self.fig_energ.itemAt(0).widget().deleteLater()
        else:    
            self.c_energ = 0
        
        self.graficar(ejes = [['vel', 'recovery', 'gait']], df = alldata, layout = self.fig_energ)
        self.guardar_energ.setEnabled(True)
        
    def fn_stiffness(self):
        self.act_rig.clicked.connect(self.actualizar_rig)
        self.pbar_stiffness.setValue(0)
        N = len(self.files)
        dfs = []
        for i in range(N):
            rigidez_df = biomec.rigidez(self.files[i])
            dfs.extend(rigidez_df)
            progress = int((i+1)/N*100)
            self.pbar_stiffness.setValue(progress)
        
        stiff = pd.concat(dfs,ignore_index=True)
        self.tabWidget.setTabEnabled(3, True)
        self.rig = stiff
        
        self.rig_eje_x_1.addItems(stiff.columns)
        self.rig_eje_y_1.addItems(stiff.columns)
        self.rig_agrupacion_1.addItems(stiff.columns)
        self.rig_eje_x_2.addItems(stiff.columns)
        self.rig_eje_y_2.addItems(stiff.columns)
        self.rig_agrupacion_2.addItems(stiff.columns)
        self.rig_eje_x_3.addItems(stiff.columns)
        self.rig_eje_y_3.addItems(stiff.columns)
        self.rig_agrupacion_3.addItems(stiff.columns)
        
        self.CB_rig_1.setChecked(True)
        self.CB_rig_2.setChecked(False)
        self.CB_rig_3.setChecked(False)
        index_1 = self.rig_eje_x_1.findText('vel')
        index_2 = self.rig_eje_y_1.findText('kleg')
        index_3 = self.rig_agrupacion_1.findText('gait')
        
        self.rig_eje_x_1.setCurrentIndex(index_1)
        self.rig_eje_x_2.setCurrentIndex(index_1)
        self.rig_eje_x_3.setCurrentIndex(index_1)
        self.rig_eje_y_1.setCurrentIndex(index_2)
        self.rig_agrupacion_1.setCurrentIndex(index_3)
        
        if self.c_rig==0:
            self.fig_rig.itemAt(0).widget().deleteLater()
        else:    
            self.c_rig = 0
        self.graficar(ejes = [['vel', 'kleg', 'gait']], df = stiff, layout = self.fig_rig)
        self.guardar_rig.setEnabled(True)

    
    def actualizar_energ(self):
        ejes = []
        if self.CB_energ_1.isChecked():
            eje_x = self.energ_eje_x_1.currentText()
            eje_y = self.energ_eje_y_1.currentText()
            agrupacion = self.energ_agrupacion_1.currentText()
            ejes.append([eje_x, eje_y, agrupacion])
            
        if self.CB_energ_2.isChecked():
            eje_x = self.energ_eje_x_2.currentText()
            eje_y = self.energ_eje_y_2.currentText()
            agrupacion = self.energ_agrupacion_2.currentText()
            ejes.append([eje_x, eje_y, agrupacion])    
        
        if self.CB_energ_3.isChecked():
            eje_x = self.energ_eje_x_3.currentText()
            eje_y = self.energ_eje_y_3.currentText()
            agrupacion = self.energ_agrupacion_3.currentText()
            ejes.append([eje_x, eje_y, agrupacion])
        
        self.fig_energ.itemAt(0).widget().deleteLater()
        self.graficar(df = self.energ, ejes = ejes, layout = self.fig_energ)
        
    def actualizar_win(self):
        ejes = []
        if self.CB_win_1.isChecked():
            eje_x = self.win_eje_x_1.currentText()
            eje_y = self.win_eje_y_1.currentText()
            agrupacion = self.win_agrupacion_1.currentText()
            ejes.append([eje_x, eje_y, agrupacion])
            
        if self.CB_win_2.isChecked():
            eje_x = self.win_eje_x_2.currentText()
            eje_y = self.win_eje_y_2.currentText()
            agrupacion = self.win_agrupacion_2.currentText()
            ejes.append([eje_x, eje_y, agrupacion])    
        
        if self.CB_win_3.isChecked():
            eje_x = self.win_eje_x_3.currentText()
            eje_y = self.win_eje_y_3.currentText()
            agrupacion = self.win_agrupacion_3.currentText()
            ejes.append([eje_x, eje_y, agrupacion])
        
        self.fig_win.itemAt(0).widget().deleteLater()
        self.graficar(df = self.win, ejes = ejes, layout = self.fig_win)
    
    def actualizar_rig(self):
        ejes = []
        if self.CB_rig_1.isChecked():
            eje_x = self.rig_eje_x_1.currentText()
            eje_y = self.rig_eje_y_1.currentText()
            agrupacion = self.rig_agrupacion_1.currentText()
            ejes.append([eje_x, eje_y, agrupacion])
            
        if self.CB_rig_2.isChecked():
            eje_x = self.rig_eje_x_2.currentText()
            eje_y = self.rig_eje_y_2.currentText()
            agrupacion = self.rig_agrupacion_2.currentText()
            ejes.append([eje_x, eje_y, agrupacion])    
        
        if self.CB_rig_3.isChecked():
            eje_x = self.rig_eje_x_3.currentText()
            eje_y = self.rig_eje_y_3.currentText()
            agrupacion = self.rig_agrupacion_3.currentText()
            ejes.append([eje_x, eje_y, agrupacion])
        
        self.fig_rig.itemAt(0).widget().deleteLater()
        self.graficar(df = self.rig, ejes = ejes, layout = self.fig_rig)
    
    def graficar(self, layout, ejes, df):
        N = len(ejes)
        fig, axes = plt.subplots(N)
        plt.close(fig)
        plt.subplots_adjust(hspace=0)
        self.canvas = FigureCanvas(fig)
        self.canvas.draw()
        layout.addWidget(self.canvas)
        if N == 1:
            sns.lineplot(ax = axes, x=ejes[0][0], y=ejes[0][1], data=df, hue=ejes[0][2], ci='sd')    
        else:
            for i in range(N):
                sns.lineplot(ax = axes[i], x=ejes[i][0], y=ejes[i][1], data=df, hue=ejes[i][2], ci='sd')
        fig.tight_layout()
        layout.update()         

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    GUI = main_GUI()
    GUI.show()
    sys.exit(app.exec_())