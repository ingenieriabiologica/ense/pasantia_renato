Para ejecutar la interfaz gráfica de Biomecánica es necesario 
tener Python 3 instalado. 

#### Paso 1: crear entorno virtual ####
> python3 -m venv gui_venv

#### Paso 2: activar el entorno virtual ####
> cd gui_venv/Scripts
> activate

#### Paso 3: instalar librerias ####
(gui_venv) > pip install -r ./requirements.txt

#### Paso 4: ejecutar la interfaz gráfica ####
(gui_venv) > python3 GUI_biomec.py

## Para generar un instalador
1) instalar pysintaller, en la consola: pip install pyinstaller
2) en el directorio de GUI_biomec.py ejecutar: pyinstaller --onefile GUI_biomec.py
