# -*- coding: utf-8 -*-
"""
Created on Fri Aug 27 00:28:40 2021

@author: Taller
"""
import numpy as np
import scipy.signal as sig
import pandas as pd

# =============================================================================
# Funciones generales
# =============================================================================

def angulos_abs(m,proximal,distal):
    #ATAN2(y proximal – y distal, z proximal – z distal)
    angle = np.arctan2(m[proximal+'y'] - m[distal+'y'], m[proximal+'z'] - m[distal+'z'])
    return angle

def angulos(m,a,b,c):
    """
    Parameters
    ----------
    m : DataFrame
        DataFrame con los marcadores.
    a : string
        Nombre del marcador.
    b : string
        Nombre del marcador.
    c : string
        Nombre del marcador sobre el cual se calcula el angulo.

    Returns
    -------
    angle : float array
        Angulo en c dados los marcadores a,b y c.
    Description
    Determina el angulo en el vértice c, formado por los marcadores a, b y c.
    """
    Ax = m[a+'x']-m[b+'x']
    Ay = m[a+'y']-m[b+'y']
    Az = m[a+'z']-m[b+'z']
    Al = np.sqrt(Ax**2+Ay**2+Az**2)

    Bx = m[a+'x']-m[c+'x']
    By = m[a+'y']-m[c+'y']
    Bz = m[a+'z']-m[c+'z']
    Bl = np.sqrt(Bx**2+By**2+Bz**2)

    Cx = m[b+'x']-m[c+'x']
    Cy = m[b+'y']-m[c+'y']
    Cz = m[b+'z']-m[c+'z']
    Cl = np.sqrt(Cx**2+Cy**2+Cz**2)

    angle = np.arccos((Al**2+Bl**2-Cl**2)/(2*Al*Bl))

    return angle

def parametros_exp(data):
    """
    Parameters
    ----------
    data : DataFrame
        Datos de cinemática desde npy.

    Returns
    -------
    m : float 
        Masa del sujeto.
    vel : float
        Velocidad del exprerimento en m/s.
    fs : int
        Freceuncia de muestro de los datos cinemáticos.
    dt : float
        Período de muestreo.
    Description
    -------
    Devuelve los metadatos contenidos en data  

    """
    m = data.get('info')['mass (kg)'].values.item()
    vel = data.get('info')['vel (km/h)'].values.item()/3.6
    fs = data.get('info')['fs_kin (Hz)'].values.item()
    gait = data.get('info')['gait'].values.item()
    subject = data.get('info')['file_name'].values.item()
    dt = 1/fs
    trail_leg = data.get('info')['trailing_leg'].values.item()
    return m, vel, fs, dt, gait, trail_leg, subject

def get_cm(model_out, win):
    """
    Parameters
    ----------
    model_out : model_otput_l
        DESCRIPTION.
    win : numpy array

    Returns
    -------
    cm : DataFrame
        Pocisión del centro de masa en mts.
    dcm : DataFrame
        Velocidad del centro de masa mts/s.
    Description
    -------
    Extrae el centro de masa y su derivada del modelo_out.   
    """
    cm = model_out[['cm_x','cm_y','cm_z']].reset_index(drop=True)/1000
    dcm = pd.DataFrame()
    dcm['x'] = sig.convolve(cm['cm_x'],win,'same')
    dcm['y'] = sig.convolve(cm['cm_y'],win,'same')
    dcm['z'] = sig.convolve(cm['cm_z'],win,'same')
    dcm.iloc[0] = dcm.iloc[1]
    dcm.iloc[-1] = dcm.iloc[-2]
    return cm, dcm

def stride_params(N, dt, vel):
    '''
    Description
    -------
    Determina parámetros de la sancada. 

    Parameters
    ----------
    N : int
        cantidad de muestras en el paso.
    dt : float
        período de muestreo.
    vel : float
        velocidad de la cinta.

    Returns
    -------
    duration : float
        Duración de la sancada.
    Freq : float
        frecuencia de sancada.
    Length : float
        distancia de la sancada.
    '''
    duration = N*dt
    Freq = 1/duration
    Length = vel/Freq
    return duration, Freq, Length

# =============================================================================
# Funciones para determinar el trabajo exeterno y la energía
# =============================================================================
def W_params_byStride(model_out, vel, dt, m, gait, subject):
    """
    Description
    -------
    Determina parametros energeticos para cada paso. 

    Parameters
    ----------
    model_out : DataFrame
        Datos de una sancada.
    vel : float
        Velocidad en m/s.
    dt : float
        Periodo de muestreo.
    m : float
        Masa en kg.

    Returns
    -------
    Energetic : DataFrame
        Parámetros energeticos: trabajo externo, recovery, trabajo vertical, trabajo horzontal.

    """
    # Centro de masa
    win = np.array([1/(2*dt),0,-1/(2*dt)])
    cm, dcm = get_cm(model_out, win)
    
    # Paso
    N = model_out.shape[0]
    stride_duration, strideFreq, strideLength = stride_params(N, dt, vel)
    
    # Energía
    E = pd.DataFrame()
    E['Eky'] = (m*(dcm['y']+vel)**2)/2
    E['Ekz'] = (m*(dcm['z']**2))/2
    E['mgh'] = m*9.8*cm['cm_z']
    E['Ez'] =  E['mgh'] + E['Ekz']
    E['Etot'] = E['Ez'] + E['Eky']
    
    # Trabajo
    wtot = E['Etot'].diff()
    wtot.iloc[0] = wtot.iloc[1]
    wz = E['Ez'].diff()
    wz.iloc[0] = wz.iloc[1]
    wy = E['Eky'].diff()
    wy.iloc[0] = wy.iloc[1]
    W = pd.DataFrame()
    W['Wtot'] = wtot*(wtot>0)
    W['Wz'] = wz*(wz>0)
    W['Wy'] = wy*(wy>0)

    Wext = W.Wtot.sum()/(strideLength*m)
    Wv = W.Wz.sum()/(strideLength*m)
    Wh = W.Wy.sum()/(strideLength*m)
    R = (1-(Wext/(Wv+Wh)))*100
    energetic = pd.DataFrame()
    energetic['recovery'] = [R]
    energetic['Wext'] = [Wext]
    energetic['Wh'] = [Wh]
    energetic['Wv'] = [Wv]
    energetic['subject'] = [subject]
    energetic['vel'] = vel*3.6
    energetic['gait'] = [gait]
    energetic['strideFreq'] = [strideFreq]
    energetic['strideLength'] = [strideLength]
    return energetic

def energetic(file):
    """
    Determina parametros energeticos para un experimento en formato .npy

    Parameters
    ----------
    file : string
        Nombre del experimento .npy.

    Returns
    -------
    energ_param : DataFrame
        Trabajo externo, recovery, trabajo vertical y trabajo horizontal para un experimento.

    """
    data = np.load(file,encoding='latin1',allow_pickle=True).item()
    
    # Extracción de parámetros del experimento
    m, vel, fs, dt, gait, trail_leg, subject= parametros_exp(data)
    dfs=[]
    for ii in data.get('model_output_l'):
        energetic = W_params_byStride(ii, vel, dt, m, gait, subject)
        dfs.append(energetic)
    
    energ_param = pd.concat(dfs,ignore_index=True)
    energ_param = energ_param.mean().to_frame().T
    energ_param['subject'] = subject
    energ_param['gait'] = gait
    return energ_param

# =============================================================================
# Funciones para el cálculo del trabajo interno
# =============================================================================
def relative(paso, SegTra, SegDel, win):
    """
    Parameters
    ----------
    paso : DataFrame
        Posición de los marcadores para un paso.
    SegTra : string list
        Segmentos del cuerpo del lado trasero.
    SegDel : string list
        Segmentos del cuerpo del lado delantero.
    win : float array
        Vector para derivar el centro de masa (convolve).

    Returns
    -------
    relativeVel : float array
        Velocidad relativa para cada frame durante un paso.

    """
    N = paso.shape[0]
    relativeCm = pd.DataFrame()
    relativeVel = pd.DataFrame()

    for seg,leg in zip([SegTra,SegDel],['_tr','_ld']):
        for se in seg:
            relativeCm[se[:-6]+leg+se[-2:]] = (paso['cm_'+se] - paso['cm_'+se[-1]])/1000
            relativeVel[se[:-6]+leg+se[-2:]] = sig.convolve(relativeCm[se[:-6]+leg+se[-2:]],win,'same')
    relativeCm.reset_index(inplace=True, drop=True)
    
    relativeVel.iloc[0] = relativeVel.iloc[1]
    relativeVel.iloc[N-1] = relativeVel.iloc[N-2]
    relativeVel.reset_index(inplace=True, drop=True)
    
    return relativeVel

def d_angles(marcadores, win, lead, trail):
    """
    Description
    ----------
    Calcula la derivada de los ángulos

    Parameters
    ----------
    marcadores : DataFrame
        Posicion de los marcadores durante un ciclo.
    win : float array
        Utilizado para derivar los ángulos (convolve).
    lead : string
        Lado delantero.
    trail : string
        Lado trasero.

    Returns
    -------
    dangles : DataFrame
        Derivada de cada angulo.

    """
    N = marcadores.shape[0]
    angles = pd.DataFrame()
    for lado,side in zip(['_ld', '_tr'],[lead.upper(),trail.upper()]):
        angles['brazo'+lado] = angulos_abs(marcadores,side+'SH', side+'ELB') # Prox - dist
        angles['antbrazo'+lado] = angulos_abs(marcadores,side+'ELB', side+'WR')
        angles['muslo'+lado] = angulos_abs(marcadores,side+'GT', side+'KN') 
        angles['pierna'+lado] = angulos_abs(marcadores,side+'KN', side+'AN')
        angles['pie'+lado] = angulos_abs(marcadores,side+'AN', side+'MT')
    angles.reset_index(inplace=True, drop=True)

    dangles = pd.DataFrame()
    for col in angles.columns:
        dangles[col] = sig.convolve(angles[col],win,'same')
    dangles.loc[0] = dangles.loc[1]
    dangles.loc[N-1] = dangles.loc[N-2]
    return dangles

def kinetic_En(dangles, m, k, relativeVelocity):
    """
    Description
    -----------
    Determina la energía cinetica por miembro para un paso.

    Parameters
    ----------
    dangles : DataFrame
        DataFrame con la derivada de los angulos entre cada segmento.
    m : DataFrame
        DataFrame con las masas ponderados de cada segmento en base a m_coef.
    k : DataFrame
        DataFrame con las **** ponderadas para cada segmento en base a k_coef.
    relativeVelocity : float array
        Velocidad relativa del centro de masa para cada frame durante un paso.

    Returns
    -------
    cineticaBylimb : DataFrame
        Energía cinética por miembro: superior e inferior, derecho e izquierdo.
    """
    cinetica  = pd.DataFrame()
    for segXYZ in relativeVelocity.columns:
        cinetica[segXYZ] = (1/2)*((relativeVelocity[segXYZ]**2)*np.float(m[segXYZ[:-2]]))
    cinetica.reset_index(inplace=True, drop=True)
    
    I = pd.DataFrame()
    for segm in m.columns:    
        I[segm] = (np.float(m[segm]))*(k[segm]**2)
        cinetica[segm+'Rot'] = (1/2)*I[segm]*(dangles[segm]**2)
     
    cineticatotal = pd.DataFrame()
    
    cineticatotal['muslo_tr'] = cinetica[['muslo_tr_z','muslo_tr_y','muslo_trRot']].sum(axis=1)#,'brazo_derRot_x']].sum(axis=1)
    cineticatotal['muslo_ld'] = cinetica[['muslo_ld_z','muslo_ld_y','muslo_ldRot']].sum(axis=1)#,'brazo_izqRot_x']].sum(axis=1)
    cineticatotal['pierna_tr'] = cinetica[['pierna_tr_z','pierna_tr_y','pierna_trRot']].sum(axis=1)#,'antbrazo_derRot_x']].sum(axis=1)
    cineticatotal['pierna_ld'] = cinetica[['pierna_ld_z','pierna_ld_y','pierna_ldRot']].sum(axis=1)#,'antbrazo_izqRot_x']].sum(axis=1)
    cineticatotal['pie_tr'] = cinetica[['pie_tr_z','pie_tr_y','pie_trRot']].sum(axis=1)#,'antbrazo_derRot_x']].sum(axis=1)
    cineticatotal['pie_ld'] = cinetica[['pie_ld_z','pie_ld_y','pie_ldRot']].sum(axis=1)
    cineticatotal['brazo_tr'] = cinetica[['brazo_tr_z','brazo_tr_y','brazo_trRot']].sum(axis=1)#,'brazo_derRot_x']].sum(axis=1)
    cineticatotal['brazo_ld'] = cinetica[['brazo_ld_z','brazo_ld_y','brazo_ldRot']].sum(axis=1)#,'brazo_izqRot_x']].sum(axis=1)
    cineticatotal['antbrazo_tr'] = cinetica[['antbrazo_tr_z','antbrazo_tr_y','antbrazo_trRot']].sum(axis=1)#,'antbrazo_derRot_x']].sum(axis=1)
    cineticatotal['antbrazo_ld'] = cinetica[['antbrazo_ld_z','antbrazo_ld_y','antbrazo_ldRot']].sum(axis=1)#,'antbrazo_izqRot_x']].sum(axis=1)
    
    
    cineticaBylimb = pd.DataFrame()
    cineticaBylimb['sup_ld'] = cineticatotal[['brazo_ld','antbrazo_ld']].sum(axis=1)
    cineticaBylimb['sup_tr'] = cineticatotal[['brazo_tr','antbrazo_tr']].sum(axis=1)
    cineticaBylimb['inf_tr'] = cineticatotal[['muslo_tr','pierna_tr','pie_tr']].sum(axis=1)
    cineticaBylimb['inf_ld'] = cineticatotal[['muslo_ld','pierna_ld','pie_ld']].sum(axis=1)
    
    return cineticaBylimb





def wint_paso(paso, marcadores, meta_data, win, WintLimbByframe, listLimbdf):
    """
    Determina el trabajo interno para cada paso y lo agrega a WintLimbByframe y a listLibmdf

    Parameters
    ----------
    paso : DataFrame
        Angulos y centros de masa para cada frame de un paso.
    marcadores : DataFrame
        Poscion de los marcadores para cada frame de un paso.
    meta_data : dict
        Datos del experimento.
    win : float array
        Para determinar la derivada.
    WintLimbByframe : listarray list
        Lista de trabajo interno para cada miembro por frame.
    listLimbdf : DataFrame list
        Lista con los datos de cada mimebro para cada paso.

    Returns
    -------
    IncrementosPositivos : DataFrame
        DESCRIPTION.
    WintLimbByframe : listarray list
        Lista de trabajo interno para cada miembro por frame.
    listLimbdf : DataFrame list
        Lista con los datos de cada mimebro para cada paso..

    """
    N = paso.shape[0]
    strideDur, strideFreq, strideLen = stride_params(N, meta_data['dt'], meta_data['vel'])
    marcadores = marcadores/1000
    k = pd.DataFrame(data = {'brazo_tr':[meta_data['k_coef'][0]]*np.sqrt((marcadores[meta_data['trail'].upper()+'ELBx']-marcadores[meta_data['trail'].upper()+'SHx'])**2+(marcadores[meta_data['trail'].upper()+'ELBy']-marcadores[meta_data['trail'].upper()+'SHy'])**2+(marcadores[meta_data['trail'].upper()+'ELBz']-marcadores[meta_data['trail'].upper()+'SHz'])**2),
                               'brazo_ld':[meta_data['k_coef'][0]]*np.sqrt((marcadores[meta_data['lead'].upper()+'ELBx']-marcadores[meta_data['lead'].upper()+'SHx'])**2+(marcadores[meta_data['lead'].upper()+'ELBy']-marcadores[meta_data['lead'].upper()+'SHy'])**2+(marcadores[meta_data['lead'].upper()+'ELBz']-marcadores[meta_data['lead'].upper()+'SHz'])**2),
                               'antbrazo_tr':[meta_data['k_coef'][1]]*np.sqrt((marcadores[meta_data['trail'].upper()+'WRx']-marcadores[meta_data['trail'].upper()+'ELBx'])**2+(marcadores[meta_data['trail'].upper()+'WRy']-marcadores[meta_data['trail'].upper()+'ELBy'])**2+(marcadores[meta_data['trail'].upper()+'WRz']-marcadores[meta_data['trail'].upper()+'ELBz'])**2),
                               'antbrazo_ld':[meta_data['k_coef'][1]]*np.sqrt((marcadores[meta_data['lead'].upper()+'WRx']-marcadores[meta_data['lead'].upper()+'ELBx'])**2+(marcadores[meta_data['lead'].upper()+'WRy']-marcadores[meta_data['lead'].upper()+'ELBy'])**2+(marcadores[meta_data['lead'].upper()+'WRz']-marcadores[meta_data['lead'].upper()+'ELBz'])**2),
                               'muslo_tr':[meta_data['k_coef'][2]]*np.sqrt((marcadores[meta_data['trail'].upper()+'KNx']-marcadores[meta_data['trail'].upper()+'GTx'])**2+(marcadores[meta_data['trail'].upper()+'KNy']-marcadores[meta_data['trail'].upper()+'GTy'])**2+(marcadores[meta_data['trail'].upper()+'KNz']-marcadores[meta_data['trail'].upper()+'GTz'])**2),
                               'muslo_ld':[meta_data['k_coef'][2]]*np.sqrt((marcadores[meta_data['lead'].upper()+'KNx']-marcadores[meta_data['lead'].upper()+'GTx'])**2+(marcadores[meta_data['lead'].upper()+'KNy']-marcadores[meta_data['lead'].upper()+'GTy'])**2+(marcadores[meta_data['lead'].upper()+'KNz']-marcadores[meta_data['lead'].upper()+'GTz'])**2),
                               'pierna_tr':[meta_data['k_coef'][3]]*np.sqrt((marcadores[meta_data['trail'].upper()+'ANx']-marcadores[meta_data['trail'].upper()+'KNx'])**2+(marcadores[meta_data['trail'].upper()+'ANy']-marcadores[meta_data['trail'].upper()+'KNy'])**2+(marcadores[meta_data['trail'].upper()+'ANz']-marcadores[meta_data['trail'].upper()+'KNz'])**2),
                               'pierna_ld':[meta_data['k_coef'][3]]*np.sqrt((marcadores[meta_data['lead'].upper()+'ANx']-marcadores[meta_data['lead'].upper()+'KNx'])**2+(marcadores[meta_data['lead'].upper()+'ANy']-marcadores[meta_data['lead'].upper()+'KNy'])**2+(marcadores[meta_data['lead'].upper()+'ANz']-marcadores[meta_data['lead'].upper()+'KNz'])**2),
                               'pie_tr':[meta_data['k_coef'][4]]*np.sqrt((marcadores[meta_data['trail'].upper()+'MTx']-marcadores[meta_data['trail'].upper()+'HEEx'])**2+(marcadores[meta_data['trail'].upper()+'MTy']-marcadores[meta_data['trail'].upper()+'HEEy'])**2+(marcadores[meta_data['trail'].upper()+'MTz']-marcadores[meta_data['trail'].upper()+'HEEz'])**2),
                               'pie_ld':[meta_data['k_coef'][4]]*np.sqrt((marcadores[meta_data['lead'].upper()+'MTx']-marcadores[meta_data['lead'].upper()+'HEEx'])**2+(marcadores[meta_data['lead'].upper()+'MTy']-marcadores[meta_data['lead'].upper()+'HEEy'])**2+(marcadores[meta_data['lead'].upper()+'MTz']-marcadores[meta_data['lead'].upper()+'HEEz'])**2)})
    k.reset_index(inplace=True, drop=True)
    
    relativeVelocity = relative(paso, meta_data['SegTra'], meta_data['SegDel'], win)

    dangles = d_angles(marcadores, win, meta_data['lead'], meta_data['trail'])
    
    cineticaBylimb =  kinetic_En(dangles, meta_data['m'], k, relativeVelocity)
 
    incrementos = cineticaBylimb.diff()
    IncrementosPositivos = ((incrementos>0)*incrementos)/(meta_data['masa']*strideLen)
    WintLimb = IncrementosPositivos.sum()
    WintLimbByframe.append(sig.resample(IncrementosPositivos.values[1:,:],100))
    pormiembro = WintLimb.to_frame().T
    pormiembro['Wint'] = WintLimb.sum()
    listLimbdf.append(pormiembro)
    
    return IncrementosPositivos, WintLimbByframe, listLimbdf

def wint(file):
    """
    Determine el trabajo interno para un experimento en formato npy

    Parameters
    ----------
    file : string
        Nombre del experimento .npy.

    Returns
    -------
    WINT : DataFrame
        Trabajo interno total.
    byframe : DataFrame
        Trbajo interno por frame.
    info : dict
        Informacion del experimento.

    """
    data = np.load(file,allow_pickle=True, encoding='latin1').item()
    segmentosXYZ = ['pierna_der_x',  'pierna_der_y', 'pierna_der_z', 
                'pierna_izq_x', 'pierna_izq_y', 'pierna_izq_z',
                'muslo_der_x', 'muslo_der_y', 'muslo_der_z', 
                'muslo_izq_x', 'muslo_izq_y', 'muslo_izq_z',
                'pie_der_x', 'pie_der_y', 'pie_der_z',
                'pie_izq_x', 'pie_izq_y', 'pie_izq_z',
                'brazo_der_x', 'brazo_der_y', 'brazo_der_z', 
                'brazo_izq_x', 'brazo_izq_y', 'brazo_izq_z',
                'antbrazo_der_x', 'antbrazo_der_y', 'antbrazo_der_z', 
                'antbrazo_izq_x', 'antbrazo_izq_y', 'antbrazo_izq_z']
    SegmentosDerechos = [s for s in segmentosXYZ if 'der' in s]
    SegmentosIzquierdos = [s for s in segmentosXYZ if 'izq' in s]
    
    # [Brazo, antebrazo, muslo, pierna, pie]
    m_coef = [0.028, 0.016, 0.1, 0.0465, 0.0145]
    k_coef = [0.322, 0.303, 0.323, 0.302, 0.475]
    
    masa, vel, fs, dt, gait, trail, subject = parametros_exp(data)
    info = data['info']
    m=pd.DataFrame(data = {'brazo_tr':[masa*m_coef[0]],
                               'brazo_ld':[masa*m_coef[0]],
                               'antbrazo_tr':[masa*m_coef[1]],
                               'antbrazo_ld':[masa*m_coef[1]],
                               'muslo_tr':[masa*m_coef[2]],
                               'muslo_ld':[masa*m_coef[2]],
                               'pierna_tr':[masa*m_coef[3]],
                               'pierna_ld':[masa*m_coef[3]],
                               'pie_tr':[masa*m_coef[4]],
                               'pie_ld':[masa*m_coef[4]]})
    # Defino ventana de convolucion
    win = np.array([1/(2*dt),0,-1/(2*dt)])
    
    listLimbdf = []

    if trail == 'r':
        trasera = 'der'
        lead = 'l'
        delantera = 'izq'
        SegTra = SegmentosDerechos
        SegDel = SegmentosIzquierdos
    else:
        trasera = 'der'
        lead = 'r'
        delantera = 'izq'
        SegDel = SegmentosDerechos
        SegTra = SegmentosIzquierdos

    WintLimbByframe = []
    meta_data = {'masa': masa, 'vel': vel, 'fs': fs, 'dt': dt, 'gait': gait, 'trail': trail,'subject': subject, 'trasera': trasera, 'lead': lead, 'delantera': delantera, 'SegTra': SegTra, 'SegDel': SegDel, 'm': m, 'k_coef': k_coef}
    
    for paso, marcadores in zip(data['model_output_'+trail][:20],data['kinematic_'+trail][:20]):
        IncrementosPositivos, WintLimbByframe, listLimbdf = wint_paso(paso, marcadores, meta_data, win, WintLimbByframe, listLimbdf)

    allstrides = pd.concat(listLimbdf,ignore_index=True)
    WINT = pd.DataFrame()
    WINT = allstrides.mean().to_frame().T
    byframe = pd.DataFrame(np.mean(WintLimbByframe,axis=0),columns=IncrementosPositivos.columns)

    WINT['subject'] = [subject]
    WINT['vel'] = [vel*3.6]
    WINT['gait'] = [gait]
    
    return WINT, byframe, info

# =============================================================================
# Funciones para determinar la rigidez
# =============================================================================

def spatiotemporal_param_byStride(cycle,fsample=100):

	morethanthreshold = cycle.iloc[10:]>cycle.min()+0.020
	toe_off = morethanthreshold[morethanthreshold==True].index[0]
	# ss = cycle.loc[toe_off]
	cycle_samples = cycle.index[-1]-cycle.index[0]
	stride_time = cycle_samples*(1/np.float(fsample))
	contact_time = (toe_off - cycle.index[0])*(1/fsample)
	flight_time = (cycle.index[-1] - toe_off)*(1/fsample)

# duty_factor = contact_time/np.float((cycle_dur))
	return contact_time,flight_time,stride_time

def rigidez(file):
    g = -9.8
    data = np.load(file,allow_pickle=True,encoding='latin1').item()
    m, v, _, _, gait, _, subject = parametros_exp(data)
    rigidez_df = []
    
    if data['info']['trailing_leg'][0] == 'r':
        zipped = zip([data['kinematic_r'],data['kinematic_l']],['RMTz','LMTz'],['tr','ld'],['RGTz','LGTz'])
    else:
        zipped = zip([data['kinematic_l'],data['kinematic_r']],['LMTz','RMTz'],['tr','ld'],['LGTz','RGTz'])

    for ll,side,LEG,cadera in zipped:
        stiffness = []
        stiffnessCM = []
        F = []
        DL = []
        DY = []
        for nn in ll:	
            matrix = nn/1000
            n = matrix[side]
            ct, ft, st = spatiotemporal_param_byStride(n)
            leg = LEG
            L = (matrix[cadera]-n).max()
            Fmax = m*g*(np.pi/2)*((ft/ct)+1)
            deltayc = ((Fmax*(ct**2))/(m*np.pi**2)) + ((g*((ct**2))/8))
            vtc2 = ((v)*ct)/2
            deltaL = L - np.sqrt((L**2)-vtc2**2) + deltayc
            # deltay = ((Fmax*ct**2)/(m*np.pi**2))+g*((ct**2)/8)
            kleg = Fmax/deltaL
            kvert = Fmax/deltayc
            stiffness.append(kleg)
            stiffnessCM.append(kvert)
            F.append(-Fmax)
            DL.append(-deltaL)
            DY.append(-deltayc)
        K = np.mean(stiffness)
        rigidez_byLeg = pd.DataFrame()
        rigidez_byLeg['subject'] = [subject[0:2]]
        rigidez_byLeg['vel'] = [v*3.6]
        rigidez_byLeg['gait'] = gait.upper()
        rigidez_byLeg['leg'] = leg
        rigidez_byLeg['kleg'] = K
        rigidez_byLeg['kvert'] = np.mean(stiffnessCM)
        rigidez_byLeg['Fmax'] = [np.mean(F)]
        rigidez_byLeg['deltaL'] = [np.mean(DL)]
        rigidez_byLeg['deltaY'] = [np.mean(DY)]
        rigidez_df.append(rigidez_byLeg)
        
    return rigidez_df