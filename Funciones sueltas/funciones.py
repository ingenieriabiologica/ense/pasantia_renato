import sys
import ViconNexus as vn
import numpy as np
from detect_peaks import detect_peaks
import matplotlib.pyplot as plt
import pandas as pd
sys.path.append( 'C:/Program Files (x86)/Vicon/Nexus2.5/SDK/Win32')
sys.path.append( 'C:/Program Files (x86)/Vicon/Nexus2.5/SDK/Python')
from ViconNexus import *
from ViconUtils import *
import os


def min_marker(name, marker, distance):
    """
    Busca el m�nimo en el marcador del talon
    Parameters
    ----------
    name : string
        Nombre del sujeto del experimento.
    marker : string
        Nombre del marcador para encontrar los minimos.
    distance : int
        Distancia estimada en muestras entre cada minimo.

    Returns
    -------
    min_t : int array
        indices donde se da cada minimo en el marcador maker.

    """
    x, y, p, e = vicon.GetTrajectory(name, marker)
    p = np.array(p)
    m = p[e]
    min_t= detect_peaks(-m, mpd = distance,show=True)

    return min_t

def kinematics(subject, markers, distance_r, distance_l, e):
    """
    Genera una data frame con los datos cinematicos del sujeto subject

    Parameters
    ----------
    subject : string
        Nombre del sujeto del experimento.
    markers : string list
        Lista con los nombres de los marcadores.
    distance_r : int
        Distancia entre cada m�nimo para el lado derecho.
    distance_l : int
        Distancia entre cada m�nimo para el lado izquierdo.
    e : ??
        .

    Returns
    -------
    model_output_cycles_r : DataFrame list
        DESCRIPTION.
    model_output_cycles_l : DataFrame list
        DESCRIPTION.
    kinematic_cycles_r : DataFrame list
        DESCRIPTION.
    kinematic_cycles_l : DataFrame list
        DESCRIPTION.

    """
    kinematic = pd.DataFrame()
    model_output= pd.DataFrame()

	for i in markers:
		x,y,z,e= vicon.GetTrajectory(name, i)
		kinematic[i+'x'] = np.array(x)[e]
		kinematic[i+'y'] = np.array(y)[e]
		kinematic[i+'z'] = np.array(z)[e]
	model_output = pd.DataFrame()
	for out in outputname:
	    ch,e = vicon.GetModelOutput(name,out)
	    model_output[out+'_x'] = np.array(ch[0])[e]
	    model_output[out+'_y'] = np.array(ch[1])[e]
	    model_output[out+'_z'] = np.array(ch[2])[e]
	
	kinematic_cycles_r = []
	kinematic_cycles_l = []
	model_output_cycles_r = []
	model_output_cycles_l = []
    
	min_t_r = min_marker(subject, markers[7], distance_r)
	min_t_l = min_marker(subject, markers[16], distance_l)

	for i in range(len(min_t_r[:-2])):
		kinematic_cycles_r.append(kinematic.iloc[min_t_r[i]:min_t_r[i+1],:])
		model_output_cycles_r.append(model_output.iloc[min_t_r[i]:min_t_r[i+1],:])
	for j in range(len(min_t_l[:-2])):
		kinematic_cycles_l.append(kinematic.iloc[min_t_l[j]:min_t_l[j+1],:])
		model_output_cycles_l.append(model_output.iloc[min_t_l[j]:min_t_l[j+1],:])
	
	return model_output_cycles_r, model_output_cycles_l, kinematic_cycles_r, kinematic_cycles_l

def emg(subject, markers, distance_r, distance_l, details):
	"""
    Extrae EMG de un experimento del Vicon

    Parameters
    ----------
    subject : string
        Nombre del sujeto.
    markers : string list
        Nombres de los marcadores.
    distance_r : int
        Distancia entre cada minimo pie derecho.
    distance_l : int
        Distancia entre cada minimo pie izquierdo
    details : string list
        Lista con los nombres de los canales utilizados en la adquisicion de EMG.

    Returns
    -------
    emg_cycles_r : TYPE
        DESCRIPTION.
    emg_cycles_l : TYPE
        DESCRIPTION.

    """
    emg = pd.DataFrame()
	emg_cycles_r=[]
	emg_cycles_l=[]

	min_t_r = min_marker(subject, markers[7], distance_r)
	min_t_l = min_marker(subject, markers[16], distance_l)

	for n_channel in range(0,len(details)-2):
		muscle_name = vicon.GetDeviceOutputDetails(1, n_channel+1)[0]
		emg[muscle_name] = vicon.GetDeviceChannelGlobal(1,n_channel+1,1)[0]
	emg = emg.loc[(emg != 0).any(axis=1)] #saca las columnas que son cero al inicio

	for i in range(len(min_t_r[:-2])):
		emg_cycles_r.append(emg.iloc[min_t_r[i]*20:min_t_r[i+1]*20,:])

	for j in range(len(min_t_l[:-2])):
		emg_cycles_l.append(emg.iloc[min_t_l[j]*20:min_t_l[j+1]*20,:])

	return emg_cycles_r, emg_cycles_l

# Conexion con vicon 
vicon = vn.ViconNexus()
# =============================================================================
#### Extraccion de par�metros
# Nombre del sujeto
name = vicon.GetSubjectNames()[0]

# Nombre de salida
outputname = vicon.GetModelOutputNames(name)   #�ngulos

# e (no se que es)
cm, e = vicon.GetModelOutput(name,'cm')

# Nombre de los marcadores
b = vicon.GetMarkerNames(name)          #marcadores

# Ubicacion y el nombre del archivo
path, file_name = vicon.GetTrialName()

# Masa
mass = np.float32(vicon.GetSubjectParam( vicon.GetSubjectNames()[0],'mass')[0])

# Frecuencia muestreo EMG
fs_emg = vicon.GetDeviceDetails(1)[2]

# Frecuencia muestreo cinematica
fs_kin = vicon.GetFrameRate()

# Detalles de los dispositivos (determinar si hay EMG o no)
device_details = vicon.GetDeviceDetails(1)[3]
# =============================================================================
#### Input de parametros 
# Velocidad del experimento
vel = np.float(input('Enter speed in km/h : '))

# Gait
gait = raw_input('Gait? (s, r or w): ')

# Trailing leg
trailing_leg = raw_input('trailing leg? (r or l): ')
# =============================================================================
print('Procesando...')

# Generaci�n de los model_output y los datos cinematicos
moc_r, moc_l, kic_r, kic_l = kinematics(name, b, 50, 50, e)

# Disccionario donde se almacena la informacion del experimento fragmentada
data = dict()

# Si device_detail != False se anade a data los datos de EMG, en otro caso no
if not device_details:
	info = pd.DataFrame([{'file_name':file_name, 'gait':gait,'vel (km/h)':vel, 'mass (kg)':mass, 'fs_kin':fs_kin, 'trailing_leg':trailing_leg}])
	data = ({'model_output_r':moc_r, 'kinematic_r':kic_r, 'model_output_l':moc_l, 'kinematic_l':kic_l, 'info':info})

else:	
	emgc_r, emgc_l= emg(name, b, 50, 50, device_details)
	info = pd.DataFrame([{'file_name':file_name, 'gait':gait,'vel (km/h)':vel, 'mass (kg)':mass, 'fs_kin (Hz)':fs_kin, 'fs_emg (Hz)':fs_emg, 'trailing_leg':trailing_leg}])
	data = ({'model_output_r':moc_r, 'kinematic_r':kic_r, 'emg_r': emgc_r, 'model_output_l':moc_l, 'kinematic_l':kic_l, 'emg_l': emgc_l, 'info':info})

directory = path+'npy\\'
if not os.path.exists(directory):
    os.makedirs(directory)

# Creaci�n del archivo .npy con los datos del experimento
np.save(directory+file_name+'.npy',data)
print('Cierre la ventana')
