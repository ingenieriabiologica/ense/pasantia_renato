# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import biomec
import glob
import sys
import seaborn as sns

if __name__ == "__main__":
    graficos = False
    if len(sys.argv) == 1:
        folder = "."
    else:
        if sys.argv[1] == 'help':
            print("Este script determina parámetros energéticos para todos los archivos .npy que se encuentren en el directorio especificado. \nForma de ejecución:python3 energetic.py <ubicación de los .npy> <ploteo>. \nSi ploteo es true se plotea los parametros calculados")
            sys.exit()
        folder = sys.argv[1]
        if len(sys.argv)==3:
            graficos = bool(sys.argv[2])

# Usar args para pasar ruta


r = []
for file in glob.glob(folder+'/*.npy'): #Recorre el directorio buscando archivos npy
    print(file)
    energ_param = biomec.energetic(file)
    print(energ_param.columns)
    r.append(energ_param)
    
alldata = pd.concat(r, ignore_index= True)
alldata.to_pickle(folder+'/energetic')

if graficos:
    f, axes = plt.subplots(2)
    plt.subplots_adjust(hspace=0)
    sns.lineplot(ax = axes[0], x='vel', y='recovery', data=alldata, hue='gait', ci='sd')
    sns.lineplot(ax = axes[1], x='vel', y='Wext', data=alldata, hue='gait', ci='sd')
    plt.show()