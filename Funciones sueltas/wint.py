# Biomecanica trabajo final


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob
import seaborn as sns
from progress.bar import FillingCirclesBar
import biomec
import sys

if __name__ == "__main__":
    graficos = False
    if len(sys.argv) == 1:
        folder = "./"
    else:
        if sys.argv[1] == 'help':
            print("Este script determina el trabajo interno para todos los archivos .npy que se encuentren en el directorio especificado. \nForma de ejecución:python3 energetic.py <ubicación de los .npy> <ploteo>. \nSi ploteo es true se plotea los parametros calculados")
            sys.exit()
        folder = sys.argv[1]
        if len(sys.argv)==3:
            graficos = bool(sys.argv[2])



archivos = glob.glob(folder+'/*.npy')
# borrar = [s for s in archivos if "GP_R_065" in s][0]
# archivos.remove(borrar)

bar = FillingCirclesBar('Processing', max=len(archivos)) #Barra de procedimiento

listdf = []
Wintbyframe = {} 

for file in archivos:
    WINT, byframe, info = biomec.wint(file)
    
    bar.next()
    
    Wintbyframe.update({info.file_name.values[0]:byframe})
    listdf.append(WINT)

bar.finish()
Wint = pd.concat(listdf,ignore_index=True)
Wint.to_pickle(folder+'/wint')

sns.lineplot(x='vel',y='Wint',data=Wint,hue='gait',ci='sd')
plt.show()